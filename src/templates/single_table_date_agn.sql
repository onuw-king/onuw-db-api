select
{% for field in query.fields %}
{% if loop.last %}
    {{ field }}
{% else %}
    {{ field }},
{% endif %}
{% endfor %}
from {{ query.db_name }}.{{ query.table_name }}
where
{% for cond in query.conditions %}
    {% if loop.first %}
        {% if not(cond.value is None) %}
    {{ cond.name }} = {{ cond.value }}
        {% endif %}
    {% else %}
        {% if not(cond.value is None) %}
    and {{ cond.name }} = {{ cond.value }}
        {% endif %}
    {% endif %}
{% endfor %}