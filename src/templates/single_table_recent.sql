select
{% for field in query.fields %}
    {% if loop.last %}
    base.{{ field }}
    {% else %}
    base.{{ field }},
    {% endif %}
{% endfor %}
from {{ query.db_name }}.{{ query.table_name }} base
where
{% for cond in query.conditions %}
    {% if loop.first %}
        {% if not(cond.value is None) %}
    base.{{ cond.name }} = {{ cond.value }}
        {% endif %}
    {% else %}
        {% if not(cond.value is None) %}
    and base.{{ cond.name }} = {{ cond.value }}
        {% endif %}
    {% endif %}
{% endfor %}
inner join (
    select
    {% for field in query.primary_key.fields %}
        {% if loop.last %}
            {% if field in query.primary_key.date_fields %}
        max({{ field }})
            {% else %}
        {{ field }}
            {% endif %}
        {% else %}
            {% if field in query.primary_key.date_fields %}
        max({{ field }}),
            {% else %}
        {{ field }},
            {% endif %}
        {% endif %}
    {% endfor %}
    from {{ query.db_name }}.{{ query.table_name }}
    group by
    {% for field in query.primary_key.fields %}
        {% if loop.last %}
        {{ field }}
        {% else %}
        {{ field }},
        {% endif %}
    {% endfor %}
    having
    {% for cond in query.primary_key.conditions %}
        {% if loop.first %}
            {% if not(cond.value is None) %}
        {{ cond.name }} = {{ cond.value }}
            {% endif %}
        {% else %}
            {% if not(cond.value is None) %}
        and {{ cond.name }} = {{ cond.value }}
            {% endif %}
        {% endif %}
    {% endfor %}
) as recent
on
{% for field in query.primary_key.date_fields%}
    {% if loop.last %}
    recent.{{ field }} = base.{{ field }}
    {% else %}
    recent.{{ field }} = base.{{ field }},
    {% endif %}
{% endfor %}