from aiomysql import (
    connect,
    create_pool,
    AbstractEventLoop,
    Connection
)

class QueryCondition:
    def __init__(self,
        name: str,
        value: str
    ):
        self.name = name
        self.value = value

class PrimaryKey:
    def __init__(self,
        fields: list,
        date_fields: list,
        conditions: list = None
    ):
        self.fields = fields
        self.date_fields = date_fields
        self.conditions = conditions

class Query:
    def __init__(self,
        db_name: str,
        table_name: str,
        fields: list,
        conditions: list,
        primary_key: PrimaryKey = None
    ):
        self.db_name = db_name
        self.table_name = table_name
        self.fields = fields
        self.conditions = conditions
        self.primary_key = primary_key

class DatabaseUser:
    def __init__(self,
        name: str,
        host: str,
        password: str,
        loop: AbstractEventLoop,
        port: int = 3306
    ):
        self.Name = name
        self.Host = host
        self.Password = password
        self.Port = port
        self.loop = loop
        self.dbname = None
        self.pool = None
    
    async def set_pool(self, dbname: str = None):
        if dbname is None and self.pool is None:
            self.pool = await create_pool(user=self.Name, host=self.Host, password=self.Password, \
                db=self.dbname, port=self.Port, loop=self.loop)
        elif not(dbname is None):
            self.pool = await create_pool(user=self.Name, host=self.Host, password=self.Password, \
                db=dbname, port=self.Port, loop=self.loop)
        else:
            raise NotImplementedError("Unintended method for creating connection pool.")

    async def close_pool(self):
        if not(self.pool is None):
            self.pool.close()
            await self.pool.wait_closed()

    async def get_connection(self, dbname: str = None):
        if not(self.pool is None):
            return await self.pool.acquire()

        if dbname is None:
            return await connect(user=self.Name, host=self.Host, password=self.Password, \
                db=self.dbname, port=self.Port, loop=self.loop)

        return await connect(user=self.Name, host=self.Host, password=self.Password, \
            db=dbname, port=self.Port, loop=self.loop)
    
    def release_connection(self, connection: Connection):
        connection.close()
        if not(self.pool is None):
            self.pool.release(connection)

