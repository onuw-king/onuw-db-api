from sys import argv
from asyncio import (
    run,
    get_running_loop
)
from pytomlpp import tomload
from fastapi import (
    FastAPI,
    Header
)
from fastapi.responses import JSONResponse
from starlette.routing import Route
from hypercorn.asyncio import serve
from hypercorn.config import Config
from jinja2 import (
    Environment,
    FileSystemLoader
)
from jinjasql import JinjaSql

from db_models import *
from db import *

class DatabaseConfig:
    def __init__(self, config: dict):
        self.username = config["db_username"]
        self.user_pw = config["db_user_pw"]
        self.ip = config["db_ip"]
        self.db_name = config["db_name"]
        self.self_id = config["self_id"]
        self.self_token = config["self_token"]
        self.client_ids = config["client_ids"]
        self.client_tokens = config["client_tokens"]

class TokenRepository:
    def __init__(self, self_id: str, self_token: str, tokens: list, client_ids: list):
        self.authorized_clients = {client_ids[i]: tokens[i] for i in range(0, len(tokens))}
        self.root_id = self_id
        self.root_token = self_token
    
    def is_valid_client(self, client_id: str, token: str):
        val = False
        if client_id in self.authorized_clients:
            if token == self.authorized_clients[client_id]:
                val = True
        return val

#input args:
#   1. db config filename
#   2. web config filename

db_config = DatabaseConfig(tomload(f"/app/config/{db_config_filename}"))
web_config = Config.from_toml(f"/app/config/{web_config_filename}")

env = Environment(
    loader = FileSystemLoader("/app/templates")
)
jinjasql = JinjaSql(env = env)

dbuser = None
token_repo = TokenRepository(
    db_config.self_id,
    db_config.self_token,
    db_config.client_ids,
    db_config.client_tokens
)

api = FastAPI()

@api.on_event("startup")
async def startup():
    global dbuser
    loop = get_running_loop()
    dbuser = DatabaseUser(
        db_config.username,
        db_config.ip,
        db_config.user_pw,
        loop
    )
    await dbuser.set_pool(db_config.db_name)

@api.on_event("shutdown")
async def shutdown():
    await dbuser.close_pool()

async def execute_query(
    query: Query,
    sql_template: str,
    constructor: type
):
    sql_code, binds = jinjasql.prepare_query(
        sql_template,
        {"query": query}
    )
    rows = None
    con = await dbuser.get_connection()
    async with con.cursor() as curs:
        await curs.execute(sql_code, binds)
        rows = await curs.fetchall()
    dbuser.release_connection(con)
    obj_list = []
    for row in rows:
        const_data = {query.fields[i]: row[i] for i in range(0, len(query.fields))}
        obj = constructor(**const_data)
        obj_list.append(obj.to_dict())
    return {"data": obj_list}

@api.get("/get/admin_roles", response_type=JSONResponse)
async def get_admin_roles(
    client_token: str = Header(None),
    client_id: str,
    when_record_created: str = None,
    server_id: str = None,
    role_id: str = None,
    is_active: int = None
):
    if not(token_repo.is_valid_client(client_id, client_token)):
        return {"error": "Invalid client."}

    conditions = [
        QueryCondition("when_record_created", when_record_created),
        QueryCondition("server_id", server_id),
        QueryCondition("role_id", role_id),
        QueryCondition("is_active", is_active)
    ]
    fields = AdminRole.fields
    query = Query(
        db_config.db_name,
        "admin_roles",
        fields,
        conditions
    )
    template = env.get_template("single_table_date_agn.sql")
    data = await execute_query(query, template, AdminRole)
    headers = {
        "root-token": token_repo.root_token,
        "root-id": token_repo.root_id
    }
    return JSONResponse(content=data, headers=headers)

@api.get("/get/players", response_type=JSONResponse)
async def get_players(
    client_token: str = Header(None),
    client_id: str,
    is_recent: int = None,
    when_game_created: str = None,
    game_master_id: str = None,
    player_id: str = None,
    player_name: str = None,
    is_in_game: int = None
):
    if not(token_repo.is_valid_client(client_id, client_token)):
        return {"error": "Invalid client."}

    conditions = [
        QueryCondition("when_game_created", when_game_created),
        QueryCondition("game_master_id", game_master_id),
        QueryCondition("player_id", player_id),
        QueryCondition("player_name", player_name),
        QueryCondition("is_in_game", is_in_game)
    ]
    fields = Player.fields
    query = None
    template = None
    if is_recent:
        key_conditions = [
            QueryCondition("game_master_id", game_master_id),
            QueryCondition("player_id", player_id),
            QueryCondition("player_name", player_name),
            QueryCondition("is_in_game", is_in_game)
        ]
        key_fields = [
            "when_game_created",
            "game_master_id",
            "player_id"
        ]
        date_fields = [
            "when_game_created"
        ]
        key = PrimaryKey(
            key_fields,
            date_fields,
            key_conditions
        )
        query = Query(
            db_config.db_name,
            "players",
            fields,
            conditions,
            key
        )
        template = env.get_template("single_table_recent.sql")
    else:
        query = Query(
            db_config.db_name,
            "players",
            fields,
            conditions
        )
        template = env.get_template("single_table_date_agn.sql")
    data = await execute_query(query, template, Player)
    headers = {
        "root-token": token_repo.root_token,
        "root-id": token_repo.root_id
    }
    return JSONResponse(content=data, headers=headers)

@api.get("/get/saved_config_metadata", response_type=JSONResponse)
async def get_saved_config_metadata(
    client_token: str = Header(None),
    client_id: str,
    is_recent: int = None,
    when_record_created: str = None,
    when_config_created: str = None,
    server_id: str = None,
    author_id: str = None,
    game_config_name: str = None,
    author_name: str = None,
    is_from_website: int = None,
    is_deleted: int = None
):
    if not(token_repo.is_valid_client(client_id, client_token)):
        return {"error": "Invalid client."}

    conditions = [
        QueryCondition("when_record_created", when_game_created),
        QueryCondition("when_config_created", when_config_created),
        QueryCondition("server_id", server_id),
        QueryCondition("author_id", author_id),
        QueryCondition("game_config_name", game_config_name),
        QueryCondition("author_name", author_name),
        QueryCondition("is_from_website", is_from_website),
        QueryCondition("is_deleted", is_deleted)
    ]
    fields = SavedConfigMetadata.fields
    query = None
    template = None
    if is_recent:
        key_conditions = [
            QueryCondition("server_id", server_id),
            QueryCondition("author_id", author_id),
            QueryCondition("game_config_name", game_config_name)
        ]
        key_fields = [
            "when_record_created",
            "server_id",
            "author_id",
            "game_config_name"
        ]
        date_fields = [
            "when_record_created"
        ]
        key = PrimaryKey(
            key_fields,
            date_fields,
            key_conditions
        )
        query = Query(
            db_config.db_name,
            "saved_config_metadata",
            fields,
            conditions,
            key
        )
        template = env.get_template("single_table_recent.sql")
    else:
        query = Query(
            db_config.db_name,
            "saved_config_metadata",
            fields,
            conditions
        )
        template = env.get_template("single_table_date_agn.sql")
    data = await execute_query(query, template, SavedConfigMetadata)
    headers = {
        "root-token": token_repo.root_token,
        "root-id": token_repo.root_id
    }
    return JSONResponse(content=data, headers=headers)

@api.get("/get/game_settings", response_type=JSONResponse)
async def get_game_settings(
    client_token: str = Header(None),
    client_id: str,
    is_recent: int = None,
    when_record_created: str = None,
    server_id: str = None,
    user_id: str = None,
    game_config_name: str = None,
    player_count: int = None,
    is_default_day_duration: int = None,
    lone_wolf: int = None,
    role_duration: int = None,
    day_duration: int = None,
    vote_duration: int = None
):
    if not(token_repo.is_valid_client(client_id, client_token)):
        return {"error": "Invalid client."}

    conditions = [
        QueryCondition("when_record_created", when_record_created),
        QueryCondition("server_id", server_id),
        QueryCondition("user_id", user_id),
        QueryCondition("game_config_name", game_config_name),
        QueryCondition("player_count", player_count),
        QueryCondition("is_default_day_duration", is_default_day_duration),
        QueryCondition("lone_wolf", lone_wolf),
        QueryCondition("role_duration", role_duration),
        QueryCondition("day_duration", day_duration),
        QueryCondition("vote_duration", vote_duration)
    ]
    fields = GameSettings.fields
    query = None
    template = None
    if is_recent:
        key_conditions = [
            QueryCondition("server_id", server_id),
            QueryCondition("user_id", author_id)
        ]
        key_fields = [
            "when_record_created",
            "server_id",
            "user_id"
        ]
        date_fields = [
            "when_record_created"
        ]
        key = PrimaryKey(
            key_fields,
            date_fields,
            key_conditions
        )
        query = Query(
            db_config.db_name,
            "game_settings",
            fields,
            conditions,
            key
        )
        template = env.get_template("single_table_recent.sql")
    else:
        query = Query(
            db_config.db_name,
            "game_settings",
            fields,
            conditions
        )
        template = env.get_template("single_table_date_agn.sql")
    data = await execute_query(query, template, GameSettings)
    headers = {
        "root-token": token_repo.root_token,
        "root-id": token_repo.root_id
    }
    return JSONResponse(content=data, headers=headers)

@api.get("/get/card_decks", response_type=JSONResponse)
async def get_card_decks(
    client_token: str = Header(None),
    client_id: str,
    is_recent: int = None,
    when_config_created: str = None,
    server_id: str = None,
    user_id: str = None,
    game_config_name: str = None,
    villager: int = None,
    werewolf: int = None,
    seer: int = None,
    robber: int = None,
    troublemaker: int = None,
    tanner: int = None,
    drunk: int = None,
    hunter: int = None,
    mason: int = None,
    insomniac: int = None,
    minion: int = None,
    doppelganger: int = None
):
    if not(token_repo.is_valid_client(client_id, client_token)):
        return {"error": "Invalid client."}

    conditions = [
        QueryCondition("when_config_created", when_config_created),
        QueryCondition("server_id", server_id),
        QueryCondition("user_id", user_id),
        QueryCondition("game_config_name", game_config_name),
        QueryCondition("villager", villager),
        QueryCondition("werewolf", werewolf),
        QueryCondition("seer", seer),
        QueryCondition("robber", robber),
        QueryCondition("troublemaker", troublemaker),
        QueryCondition("tanner", tanner),
        QueryCondition("drunk", drunk),
        QueryCondition("hunter", hunter),
        QueryCondition("mason", mason),
        QueryCondition("insomniac", insomniac),
        QueryCondition("minion", minion),
        QueryCondition("doppelganger", doppelganger)
    ]
    fields = CardDeck.fields
    query = None
    template = None
    if is_recent:
        key_conditions = [
            QueryCondition("server_id", server_id),
            QueryCondition("user_id", author_id)
        ]
        key_fields = [
            "when_config_created",
            "server_id",
            "user_id"
        ]
        date_fields = [
            "when_config_created"
        ]
        key = PrimaryKey(
            key_fields,
            date_fields,
            key_conditions
        )
        query = Query(
            db_config.db_name,
            "card_decks",
            fields,
            conditions,
            key
        )
        template = env.get_template("single_table_recent.sql")
    else:
        query = Query(
            db_config.db_name,
            "card_decks",
            fields,
            conditions
        )
        template = env.get_template("single_table_date_agn.sql")
    data = await execute_query(query, template, CardDeck)
    headers = {
        "root-token": token_repo.root_token,
        "root-id": token_repo.root_id
    }
    return JSONResponse(content=data, headers=headers)

@api.get("/get/game_metadata", response_type=JSONResponse)
async def get_game_metadata(
    client_token: str = Header(None),
    client_id: str,
    is_recent: int = None,
    when_record_created: str = None,
    server_id: str = None,
    game_master_id: str = None,
    channel_id: str = None,
    game_config_name: str = None,
    game_master_name: str = None,
    is_active: int = None,
    is_in_session: int = None
):
    if not(token_repo.is_valid_client(client_id, client_token)):
        return {"error": "Invalid client."}

    conditions = [
        QueryCondition("when_record_created", when_record_created),
        QueryCondition("server_id", server_id),
        QueryCondition("game_master_id", game_master_id),
        QueryCondition("channel_id", channel_id),
        QueryCondition("game_config_name", game_config_name),
        QueryCondition("game_master_name", game_master_name),
        QueryCondition("is_active", is_active),
        QueryCondition("is_in_session", is_in_session)
    ]
    fields = GameMetadata.fields
    query = None
    template = None
    if is_recent:
        key_conditions = [
            QueryCondition("server_id", server_id),
            QueryCondition("game_master_id", game_master_id),
            QueryCondition("channel_id", channel_id)
        ]
        key_fields = [
            "when_record_created",
            "server_id",
            "game_master_id",
            "channel_id"
        ]
        date_fields = [
            "when_record_created"
        ]
        key = PrimaryKey(
            key_fields,
            date_fields,
            key_conditions
        )
        query = Query(
            db_config.db_name,
            "game_metadata",
            fields,
            conditions,
            key
        )
        template = env.get_template("single_table_recent.sql")
    else:
        query = Query(
            db_config.db_name,
            "game_metadata",
            fields,
            conditions
        )
        template = env.get_template("single_table_date_agn.sql")
    data = await execute_query(query, template, CardDeck)
    headers = {
        "root-token": token_repo.root_token,
        "root-id": token_repo.root_id
    }
    return JSONResponse(content=data, headers=headers)

@api.get("/get/game", response_type=JSONResponse)
async def get_game(
    client_token: str = Header(None),
    client_id: str,
    is_recent: int = None,
    when_record_created: str = None,
    server_id: str = None,
    game_master_id: str = None,
    game_config_name: str = None,
    is_active: int = None,
    is_in_session: int = None
):
    if not(token_repo.is_valid_client(client_id, client_token)):
        return {"error": "Invalid client."}
    
    is_saved_config = False
    if (
        not(server_id is None)
        and not(game_master_id is None)
        and not(game_config_name is None)
    ):
        is_saved_config = True

    metadata = None
    settings = None
    card_deck = None
    settings_conditions = None
    card_deck_conditions = None
    agnostic_template = env.get_template("single_table_date_agn.sql")
    if is_saved_config:
        metadata_conditions = [
            QueryCondition("server_id", server_id),
            QueryCondition("game_master_id", game_master_id),
            QueryCondition("channel_id", channel_id),
            QueryCondition("game_config_name", game_config_name)
        ]
        metadata_key_fields = [
            "when_record_created",
            "server_id",
            "game_master_id",
            "channel_id",
            "game_config_name"
        ]
        metadata_key_date_fields = [
            "when_record_created"
        ]
        metadata_key = PrimaryKey(
            metadata_key_fields,
            metadata_date_fields,
            metadata_conditions
        )
        metadata_query = Query(
            db_config.db_name,
            "game_metadata",
            GameMetadata.fields,
            metadata_conditions,
            key
        )
        metadata_template = env.get_template("single_table_recent.sql")
        metadata = await execute_query(metadata_query, metadata_template, GameMetadata)
        when_game_created = metadata.when_record_created
        settings_conditions = [
            QueryCondition("server_id", server_id),
            QueryCondition("user_id", game_master_id),
            QueryCondition("game_config_name", game_config_name)
        ]
        card_deck_conditions = [
            QueryCondition("server_id", server_id),
            QueryCondition("user_id", game_master_id),
            QueryCondition("game_config_name", game_config_name)
        ]
    else:
        if is_recent:
            metadata_conditions = [
                QueryCondition("server_id", server_id),
                QueryCondition("game_master_id", game_master_id),
                QueryCondition("channel_id", channel_id),
                QueryCondition("is_active", is_active),
                QueryCondition("is_in_session", is_in_session)
            ]
            metadata_key_fields = [
                "when_record_created",
                "server_id",
                "game_master_id",
                "channel_id"
            ]
            metadata_key_date_fields = [
                "when_record_created"
            ]
            metadata_key = PrimaryKey(
                metadata_key_fields,
                metadata_date_fields,
                metadata_conditions
            )
            metadata_query = Query(
                db_config.db_name,
                "game_metadata",
                GameMetadata.fields,
                metadata_conditions,
                key
            )
            metadata_template = env.get_template("single_table_recent.sql")
            metadata = await execute_query(metadata_query, metadata_template, GameMetadata)
            when_game_created = metadata.when_record_created
            settings_conditions = [
                QueryCondition("when_game_created", when_game_created),
                QueryCondition("server_id", server_id),
                QueryCondition("user_id", game_master_id)
            ]
            card_deck_conditions = [
                QueryCondition("when_config_created", when_game_created),
                QueryCondition("server_id", server_id),
                QueryCondition("user_id", game_master_id)
            ]
        else:
            metadata_conditions = [
                QueryCondition("when_record_created", when_record_created)
                QueryCondition("server_id", server_id),
                QueryCondition("game_master_id", game_master_id),
                QueryCondition("channel_id", channel_id)
            ]
            settings_conditions = [
                QueryCondition("when_game_created", when_record_created),
                QueryCondition("server_id", server_id),
                QueryCondition("user_id", game_master_id)
            ]
            card_deck_conditions = [
                QueryCondition("when_config_created", when_record_created),
                QueryCondition("server_id", server_id),
                QueryCondition("user_id", game_master_id)
            ]
            metadata_query = Query(
                db_config.db_name,
                "game_metadata",
                GameMetadata.fields,
                metadata_conditions
            )
            metadata = await execute_query(metadata_query, agnostic_template, GameMetadata)
    settings_query = Query(
        db_config.db_name,
        "game_settings",
        GameSettings.fields,
        settings_conditions
    )
    card_deck_query = Query(
        db_config.db_name,
        "card_deck",
        CardDeck.fields,
        card_deck_conditions
    )
    settings = await execute_query(settings_query, agnostic_template, GameSettings)
    card_deck = await execute_query(card_deck_query, agnostic_template, CardDeck)
    game = Game(metadata, settings, card_deck)
    headers = {
        "root-token": token_repo.root_token,
        "root-id": token_repo.root_id
    }
    return JSONResponse(content=game.to_dict(), headers=headers)

@api.get("/get/saved_config", response_type=JSONResponse)
async def get_saved_config(
    client_token: str = Header(None),
    client_id: str,
    is_recent: int = None,
    when_config_created: str = None,
    server_id: str = None,
    author_id: str = None,
    game_config_name: str = None,
    is_deleted: int = None
):
    if not(token_repo.is_valid_client(client_id, client_token)):
        return {"error": "Invalid client."}

    metadata = None
    settings = None
    card_deck = None
    settings_conditions = None
    card_deck_conditions = None
    agnostic_template = env.get_template("single_table_date_agn.sql")
    if is_recent:
        metadata_conditions = [
            QueryCondition("server_id", server_id),
            QueryCondition("author_id", author_id),
            QueryCondition("game_config_name", game_config_name),
            QueryCondition("is_deleted", is_deleted)
        ]
        metadata_key_fields = [
            "server_id",
            "game_master_id",
            "channel_id"
        ]
        metadata_key_date_fields = [
            "when_record_created"
        ]
        metadata_key = PrimaryKey(
            metadata_key_fields,
            metadata_date_fields,
            metadata_conditions
        )
        metadata_query = Query(
            db_config.db_name,
            "game_metadata",
            GameMetadata.fields,
            metadata_conditions,
            key
        )
        metadata_template = env.get_template("single_table_recent.sql")
        metadata = await execute_query(metadata_query, metadata_template, GameMetadata)
        when_game_created = metadata.when_record_created
        settings_conditions = [
            QueryCondition("when_game_created", when_game_created),
            QueryCondition("author_id", author_id),
            QueryCondition("game_config_name", game_config_name),
            QueryCondition("is_deleted", is_deleted)
        ]
        card_deck_conditions = [
            QueryCondition("when_config_created", when_game_created),
            QueryCondition("server_id", server_id),
            QueryCondition("user_id", game_master_id)
        ]
    else:
        metadata_conditions = [
            QueryCondition("when_config_created", when_config_created)
            QueryCondition("server_id", server_id),
            QueryCondition("game_master_id", game_master_id),
            QueryCondition("channel_id", channel_id)
        ]
        settings_conditions = [
            QueryCondition("when_record_created", when_config_created),
            QueryCondition("server_id", server_id),
            QueryCondition("user_id", game_master_id)
        ]
        card_deck_conditions = [
            QueryCondition("when_config_created", when_config_created),
            QueryCondition("server_id", server_id),
            QueryCondition("user_id", game_master_id)
        ]
        metadata_query = Query(
            db_config.db_name,
            "game_metadata",
            GameMetadata.fields,
            metadata_conditions
        )
        metadata = await execute_query(metadata_query, agnostic_template, GameMetadata)
    settings_query = Query(
        db_config.db_name,
        "game_settings",
        GameSettings.fields,
        settings_conditions
    )
    card_deck_query = Query(
        db_config.db_name,
        "card_deck",
        CardDeck.fields,
        card_deck_conditions
    )
    settings = await execute_query(settings_query, agnostic_template, GameSettings)
    card_deck = await execute_query(card_deck_query, agnostic_template, CardDeck)
    game = Game(metadata, settings, card_deck)
    headers = {
        "root-token": token_repo.root_token,
        "root-id": token_repo.root_id
    }
    return JSONResponse(content=game.to_dict(), headers=headers)

run(serve(api, web_config))