FROM python:3.8-slim
ENV APP="/app"
ENV INSTALL="/install"
ENV LOC="/root/.local"
ENV PATH $PATH:${LOC}:${LOC}/bin

#install all dependencies
COPY ./requirements.txt ${APP}/requirements.txt
RUN pip3 install --user -r ${APP}/requirements.txt
RUN mkdir ${APP}/certs
RUN mkdir ${APP}/config

COPY ./src/ ${APP}
COPY ./src/models/ ${APP}/models

WORKDIR ${APP}
EXPOSE 443
EXPOSE 3306
#start the service
ENTRYPOINT [ "python3", "main.py" ]