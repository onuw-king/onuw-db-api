class BaseModel:
    fields = []

    def to_dict(self):
        all_vars = vars(self)
        all_vars.pop("fields", None)
        for key, val in all_vars.items():
            if isinstance(val, BaseModel):
                all_vars[key] = val.to_dict()
        return all_vars

    def to_columns(self):
        all_vars = vars(self)
        all_vars.pop("fields", None)
        for key, val in all_vars.items():
            if isinstance(val, BaseModel):
                all_vars.pop(key, None)
        return all_vars

class AdminRole(BaseModel):
    fields = [
        "when_record_created",
        "server_id",
        "role_id",
        "is_active"
    ]

    def __init__(self,
        when_record_created: str = None,
        server_id: str = None,
        role_id: str = None,
        is_active: int = None
    ):
        self.when_record_created = when_record_created
        self.server_id = server_id
        self.role_id = role_id
        self.is_active = is_active

class Player(BaseModel):
    fields = [
        "when_game_created",
        "game_master_id",
        "player_id",
        "player_name",
        "is_in_game"
    ]

    def __init__(self,
        when_game_created: str = None,
        game_master_id: str = None,
        player_id: str = None,
        player_name: str = None,
        is_in_game: int = None
    ):
        self.when_game_created = when_game_created
        self.game_master_id = game_master_id
        self.player_id = player_id
        self.player_name = player_name
        self.is_in_game = is_in_game

class SavedConfigMetadata(BaseModel):
    fields = [
        "when_record_created",
        "when_config_created",
        "server_id",
        "author_id",
        "game_config_name",
        "author_name",
        "is_from_website",
        "is_deleted"
    ]

    def __init__(self,
        when_record_created: str = None,
        when_config_created: str = None,
        server_id: str = None,
        author_id: str = None,
        game_config_name: str = None,
        author_name: str = None,
        is_from_website: int = None,
        is_deleted: int = None
    ):
        self.when_record_created = when_record_created
        self.when_config_created = when_config_created
        self.server_id = server_id
        self.author_id = author_id
        self.game_config_name = game_config_name
        self.author_name = author_name
        self.is_from_website = is_from_website
        self.is_deleted = is_deleted

class GameSettings(BaseModel):
    fields = [
        "when_record_created",
        "server_id",
        "user_id",
        "game_config_name",
        "player_count",
        "is_default_day_duration",
        "lone_wolf",
        "role_duration",
        "day_duration",
        "vote_duration"
    ]

    def __init__(self,
        when_record_created: str = None,
        server_id: str = None,
        user_id: str = None,
        game_config_name: str = None,
        player_count: int = None,
        is_default_day_duration: int = None,
        lone_wolf: int = None,
        role_duration: int = None,
        day_duration: int = None,
        vote_duration: int = None
    ):
        self.when_record_created = when_record_created
        self.server_id = server_id
        self.user_id = user_id
        self.game_config_name = game_config_name
        self.player_count = player_count
        self.is_default_day_duration = is_default_day_duration
        self.lone_wolf = lone_wolf
        self.role_duration = role_duration
        self.day_duration = day_duration
        self.vote_duration = vote_duration

class CardDeck(BaseModel):
    fields = [
        "when_config_created",
        "server_id",
        "user_id",
        "game_config_name",
        "villager",
        "werewolf",
        "seer",
        "robber",
        "troublemaker",
        "tanner",
        "drunk",
        "hunter",
        "mason",
        "insomniac",
        "minion",
        "doppelganger"
    ]

    def __init__(self,
        when_config_created: str = None,
        server_id: str = None,
        user_id: str = None,
        game_config_name: str = None,
        villager: int = None,
        werewolf: int = None,
        seer: int = None,
        robber: int = None,
        troublemaker: int = None,
        tanner: int = None,
        drunk: int = None,
        hunter: int = None,
        mason: int = None,
        insomniac: int = None,
        minion: int = None,
        doppelganger: int = None
    ):
        self.when_config_created = when_config_created
        self.server_id = server_id
        self.user_id = user_id
        self.game_config_name = game_config_name
        self.villager = villager
        self.werewolf = werewolf
        self.seer = seer
        self.robber = robber
        self.troublemaker = troublemaker
        self.tanner = tanner
        self.drunk = drunk
        self.hunter = hunter
        self.mason = mason
        self.insomniac = insomniac
        self.minion = minion
        self.doppelganger = doppelganger

class GameMetadata(BaseModel):
    fields = [
        "when_record_created",
        "server_id",
        "game_master_id",
        "channel_id",
        "game_config_name",
        "game_master_name",
        "is_active",
        "is_in_session"
    ]

    def __init__(self,
        when_record_created: str = None,
        server_id: str = None,
        game_master_id: str = None,
        channel_id: str = None,
        game_config_name: str = None,
        game_master_name: str = None,
        is_active: int = None,
        is_in_session: int = None
    ):
        self.when_record_created = when_record_created
        self.server_id = server_id
        self.game_master_id = game_master_id
        self.channel_id = channel_id
        self.game_config_name = game_config_name
        self.game_master_name = game_master_name
        self.is_active = is_active
        self.is_in_session = is_in_session

class SavedConfig(BaseModel):
    def __init__(
        metadata: dict = None,
        settings: dict = None,
        card_deck: dict = None
    ):
        if metadata is None:
            self.metadata = SavedConfigMetadata()
        else:
            self.metadata = SavedConfigMetadata(**metadata)
        if settings is None:
            self.settings = GameSettings()
        else:
            self.settings = GameSettings(**settings)
        if card_deck is None:
            self.card_deck = CardDeck()
        else:
            self.card_deck = CardDeck(**card_deck)

class Game(BaseModel):
    def __init__(self,
        metadata: dict = None,
        settings: dict = None,
        card_deck: dict = None
    ):
        if metadata is None:
            self.metadata = GameMetadata()
        else:
            self.metadata = GameMetadata(**metadata)
        if settings is None:
            self.settings = GameSettings()
        else:
            self.settings = GameSettings(**settings)
        if card_deck is None:
            self.card_deck = CardDeck()
        else:
            self.card_deck = CardDeck(**card_deck)